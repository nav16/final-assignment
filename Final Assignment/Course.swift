//
//  Course.swift
//  Final Assignment
//
//  Created by Navpreet Kaur on 2017-04-20.
//  Copyright © 2017 Navpreet Kaur. All rights reserved.
//

import Foundation

public class Course {
    
    public var name = String()
    public var grade = Int()
    
    init(n: String, g: Int){
        self.name = n
        self.grade = g
    }
    
}