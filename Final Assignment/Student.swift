//
//  Student.swift
//  Final Assignment
//
//  Created by Navpreet Kaur on 2017-04-20.
//  Copyright © 2017 Navpreet Kaur. All rights reserved.
//

import Foundation


class Student {
    
    public var name = String()
    public var courses = [Course]()
    public var averageGrade = Int()
    
    init(n: String, c: [Course]){
        self.name = n
        self.courses = c
        
        calculateAverageGrade()
    }
    
    func calculateAverageGrade(){
        var accumulated = 0
        for(var i=0;i<courses.count;i++){
            accumulated+=courses[i].grade
        }
        
        averageGrade = accumulated/courses.count;
    }
    
}
