//
//  StudentsTableViewController.swift
//  Final Assignment
//
//  Created by Navpreet Kaur on 2017-04-20.
//  Copyright © 2017 Navpreet Kaur. All rights reserved.
//

import UIKit

class StudentsTableViewController: UITableViewController {

    private var students = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadStudents()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func loadStudents(){
        
        students.append(Student(n: "Prabhu Deva",c: loadCurses()))
        students.append(Student(n: "Bahubali",c: loadCurses()))
        students.append(Student(n: "Katappa",c: loadCurses()))
        students.append(Student(n: "Dhoom II : Dhoom Again",c: loadCurses()))
        students.append(Student(n: "Devsena",c: loadCurses()))
        
        
    }
    
    func loadCurses() -> [Course]{
        var cursesQtty=5;
        var curses=[Course]()
        for(var i = 0; i < cursesQtty;i++){
            var grade = Int(arc4random_uniform(100) + 1)
            curses.append(Course(n: "Course "+String(i),g: grade))
        }
        return curses
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return students.count
    }

    //*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()//tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        cell.textLabel?.text="Name: "+students[indexPath.row].name + " Total Grade: " + String(students[indexPath.row].averageGrade)
        // Configure the cell...

        return cell
    }

}
